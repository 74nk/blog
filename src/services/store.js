import store from 'store'
import uuid from 'uuid'

export const getDate = () => new Date().toLocaleDateString();

const posts = [
    {   
        date: getDate(),
        title: 'Post 1',
        slug: 'post-1',
        tags: ['some', 'tags', 'post'],
        comments: [],
        content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error, possimus, perferendis, sit, non voluptas veritatis eos magnam eum voluptates facere commodi repellendus modi tenetur. Omnis, reiciendis architecto quis repellendus tenetur!'
    },
    {
        date: getDate(),
        title: 'Post 2',
        slug: 'post-2',
        tags: ['some', 'tags', 'other', 'post'],
        comments: [],
        content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error, possimus, perferendis, sit, non voluptas veritatis eos magnam eum voluptates facere commodi repellendus modi tenetur. Omnis, reiciendis architecto quis repellendus tenetur!'
    },
    {
        date: getDate(),
        title: 'Post 3',
        slug: 'post-3',
        tags: ['some', 'tags', 'other'],
        comments: [],
        content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error, possimus, perferendis, sit, non voluptas veritatis eos magnam eum voluptates facere commodi repellendus modi tenetur. Omnis, reiciendis architecto quis repellendus tenetur!'
    },
    {
        date: getDate(),
        title: 'Post 4',
        slug: 'post-4',
        tags: ['some', 'tags', 'other'],
        comments: [],
        content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error, possimus, perferendis, sit, non voluptas veritatis eos magnam eum voluptates facere commodi repellendus modi tenetur. Omnis, reiciendis architecto quis repellendus tenetur!'
    }
];

if(!store.get('posts')) {
    // set fixtures
    store.set('posts', posts);
}

class Store {
    getPosts() {
        return store.get('posts') || [];
    }

    findPosts(params) {
        return this.getPosts().filter(post => this.checkPost(post, params))
    }

    checkPost(post, params) {
        return Object.keys(params).every(key => this.match(post[key], params[key]))
    }

    /**
     * Match value to filter
     * @param values - value can be string or Array of values
     * @param filters - collection of filter params
     * @returns {boolean}
     */
    match(values, filters) {
        let value = [].concat(values).join(', ');
        return !![].concat(filters).every(filter => value.match(new RegExp(filter, 'i')))
    }

    getPost(slug) {
        return this.getPosts().filter(post => post.slug === slug)[0];
    }
    
    save(post) {
        let posts = this.getPosts() || [];
        let _post = posts.filter(it => it.slug === post.slug)[0];

        post.tags = post.tags.map(tag => tag.trim()).filter(tag => !!tag);
        
        if(_post) {
            Object.assign(_post, post);
        }
        else {
            posts.push(post)
        }

        store.set('posts', posts);
    }
    
    getSlug(title) {
        
    }
    
    createComment() {
        return {
            id: uuid.v4(),
            date: getDate(),
            user: '',
            content: ''
        }
    }
    
    createPost() {
        return {
            date: getDate(),
            title: 'New Post',
            slug: '',
            content: '',
            tags: [],
            comments: []
        }
    }
}

export default new Store