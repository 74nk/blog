import Vue from 'vue'
import Router from 'vue-router'
import configRouter from './routes'
import App from './App.vue'

Vue.config.debug = true;

Vue.use(Router);

const router = new Router({
    // mode: 'html5',
    // hashbang: false,
    // history: true,
    // saveScrollPosition: true
});

configRouter(router);

router.start({components: {App}}, 'body');