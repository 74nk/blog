import Posts from './components/Posts.vue'
import Post from './components/Post.vue'
import EditPost from './components/EditPost.vue'
import PageNotFound from './components/PageNotFound.vue'

export default router => {
    router.alias({
        '/': '/posts'
    });

    router.redirect({
        '*': 'page-not-found'
    });

    router.map({
        '/posts': {
            name: 'posts',
            component: Posts
        },

        '/posts/:slug': {
            name: 'post',
            component: Post
        },

        '/posts/:slug/edit': {
            name: 'edit-post',
            component: EditPost
        },

        '/page-not-found': {
            name: 'pageNotFound',
            component: PageNotFound
        }
    });
}